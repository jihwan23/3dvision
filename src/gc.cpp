#include "../../lib/max_flow/graph.h"
#include <iostream>
#include <array>
#include <vector>

int main()
{
    typedef Graph<int,int,int> GraphType;
    GraphType *g = new GraphType(/*estimated # of nodes*/ 2, /*estimated # of edges*/ 1);

    g -> add_node();
    g -> add_node();

    g -> add_tweights( 0,   /* capacities */  3, 5 );
    g -> add_tweights( 1,   /* capacities */  6, 2 );
    g -> add_edge( 0, 1,    /* capacities */  7, 7 );
    g -> add_edge( 0, 1,    /* capacities */  -7, -7 );

    int flow = g -> maxflow();


    std::cout << "Flow =" << flow << std::endl;
    std::cout << ("Minimum cut:\n");
    if (g->what_segment(0) == GraphType::SOURCE)
        std::cout << ("node0 is in the SOURCE set\n");
    else
        std::cout << ("node0 is in the SINK set\n");
    if (g->what_segment(1) == GraphType::SOURCE)
        std::cout << ("node1 is in the SOURCE set\n");
    else
        std::cout << ("node1 is in the SINK set\n");

    delete g;


    std::cout << g ->get_node_num() << std::endl;
    std::array<int,4> temp;
    std:: cout << temp.size() << std::endl;

    return 0;
}
