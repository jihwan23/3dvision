# README #

This package is created for the 3D Vision project Spring Semester 2016 on ETH. Group member Jihwan Youn, Jiadong Guo and Matteo Ruello, under supervision from Nikolay Savinov. The detailed report can be found at https://www.overleaf.com/read/vsxxwwpgzwzf


### What is this repository for? ###
* Quick summary

The repository implemented the full model of an discrete optimization technique using ray potential. from depth map, the package is able to produce a surface from Delaunay triangulation of the point cloud. it produces the .obj file consists of optimized surface area and diverse measurement including accuracy and completeness results. 

### How do I get set up? ###

* input data must consist depthmaps from different view angle. tested on P11 fountain dataset.
* Dependencies
CMake, CGAL, Boost, openCV, Eigen, FLANN

* Database configuration
* to run:
change the input and result folder path at main.cpp
* Deployment instructions


### Who do I talk to? ###

* Created by Jihwan Youn and Jiadong Guo
* Supervisor: Nikolay Savinov