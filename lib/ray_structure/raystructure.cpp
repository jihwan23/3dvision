#include "raystructure.h"
#include <iostream>
#include <fstream>
#include <random>
#include <vector>


namespace RS
{

    const std::vector<PSL::DepthMap<float,double> > load_depthmaps(const std::string& folder_depthmap, PSL::DepthMap<float,double> tmpMap, const int NUM_DEPTHMAPS) {

        std::vector< PSL::DepthMap<float,double> > depthMaps;
        std::string pathDepthMap;

        int count = 0;

        for (int i=0; i<NUM_DEPTHMAPS; i++) {
            count += 1;
            if (i<10) {
                pathDepthMap = folder_depthmap + "000" + std::to_string(i) + "_depth_map.dat";
            }
            else if (i < 100) {
                pathDepthMap = folder_depthmap + "00"  + std::to_string(i) + "_depth_map.dat";
            }
            else if (i < 1000) {
                pathDepthMap = folder_depthmap + "0"   + std::to_string(i) + "_depth_map.dat";
            }
            tmpMap.loadFromDataFile(pathDepthMap);
            depthMaps.push_back(tmpMap);
        }
        std::cout << count << " depth maps loaded" << std::endl;

        return depthMaps;
    }


    const std::vector<Vertex> get_pointcloud(const std::vector< PSL::DepthMap<float,double> > depthMaps, const int NUM_DEPTHMAPS, const std::string& folder_bbox) {

        std::vector<Vertex> pcd;
        std::string path_bbox;

        for (int i=0; i<NUM_DEPTHMAPS; i++) {
            if (i<10) {
                path_bbox = folder_bbox + "000" + std::to_string(i) + ".png.bounding";
            }
            else if (i<100) {
                path_bbox = folder_bbox + "00"  + std::to_string(i) + ".png.bounding";
            }
            else if (i<1000) {
                path_bbox = folder_bbox + "0"   + std::to_string(i) + ".png.bounding";
            }

            // check if the points are in the bounding box or not
            Eigen::Matrix<double,2,3> bbox = RS::load_bbox(path_bbox);
            for (int x=0; x<depthMaps[i].getWidth(); x++) {
                for (int y=0; y<depthMaps[i].getHeight(); y++) {
                    Eigen::Vector4d point = depthMaps[i].unproject(x,y);
                    if ( point(0,0)>bbox(0,0) && point(0,0)<bbox(1,0) && point(1,0)>bbox(0,1) && point(1,0)<bbox(1,1) && point(2,0)>bbox(0,2) && point(2,0)<bbox(1,2)) {
                        if (point(0,0)!=0 && point(0,1)!=0 && point(0,2)!=0)
                            pcd.push_back(Vertex(Point(point(0,0),point(1,0),point(2,0)),i));

                    }
                }
            }
        }
        return pcd;
    }

    const Eigen::Matrix<double,2,3> load_bbox(const std::string &path) {

        std::ifstream fin(path);
        Eigen::Matrix<double,2,3> Bbox = Eigen::MatrixXd::Zero(2,3);

        for (int k=0; k<2; ++k) {
            for (int j=0; j<3; ++j) {
                fin >> Bbox(k, j);
            }
        }
        return Bbox;
    }

    void writeOBJ(const std::string &folder, const std::string &filename, const Eigen::MatrixXd &coord_vertex, const std::vector <std::array<int,3>> &surface) {

        // set file name
        std::string path = folder+filename+".obj";
        std::ofstream os;
        os.open(path);

        if (!os.is_open()) {
            std::cout << "Outputstream not open." << std::endl;
        }

        // write file
        // vertex
        for (int i=0; i<coord_vertex.cols(); ++i)
            os << "v " << coord_vertex(0,i) << " " << coord_vertex(1,i) << " " << coord_vertex(2,i) << std::endl;
        // facet
        for (int i=0; i<surface.size(); ++i)
            os << "f " << surface[i][0]+1 << " " << surface[i][1]+1 << " " << surface[i][2]+1 << std::endl;

        os.close();
    }

    void save_pointcloud(std::vector<Vertex> &pcd, const std::string& folder, const std::string& filename, const int step) {

        // set file name
        std::string path = folder+filename+".wrl";
        std::ofstream os;

        os.open(path);
        if (!os.is_open()) {
            std::cout << "Outputstream not open." << std::endl;
        }

        int numPts = pcd.size();

        // write file
        os << "#VRML V2.0 utf8" << std::endl;
        os << "Shape {" << std::endl;
        os << "     appearance Appearance {" << std::endl;
        os << "         material Material { " << std::endl;
        os << "             diffuseColor     0.5 0.5 0.5" << std::endl;
        os << "         }" << std::endl;
        os << "     }" << std::endl;
        os << "     geometry PointSet {" << std::endl;
        os << "       coord Coordinate {" << std::endl;
        os << "           point [" << std::endl;
        for (unsigned int i = 0; i < numPts; i+=step)
            os << "               " << pcd[i].get_point(0) << " " << pcd[i].get_point(1) << " " << pcd[i].get_point(2) << "," << std::endl;
        os << "           ]" << std::endl;
        os << "       }" << std::endl;
        os << "       color Color {" << std::endl;
        os << "         color [" << std::endl;
        for (unsigned int i = 0; i < numPts; i++)
            os << "           " << 255.0 << " " << 255.0 << " " << 255.0 << "," << std::endl;

        os << "         ]" << std::endl;
        os << "       }" << std::endl;
        os << "   }" << std::endl;
        os << "}" << std::endl;
        os.close();
    }

    void save_index_vertex_per_cell(Eigen::MatrixXi &index, const std::string& folder, const std::string& filename) {

        // set file name
        std::string path = folder + filename + ".txt";
        std::ofstream os;

        os.open(path);
        if (!os.is_open()) {
            std::cout << "Outputstream not open." << std::endl;
        }

        std::cout << "Path: " << path << std::endl;

        // write file
        for (int j=0; j<index.cols(); ++j) {
            for (int i=0; i<index.rows(); ++i) {
                if (i!=index.rows()-1) {
                    os << index(i,j) << " ";
                }
                else {
                    if (j!=index.cols()-1)
                        os << index(i,j) << std::endl;
                    else
                        os << index(i,j);
                }
            }
        }
        os.close();
    }

    void save_coord_vertex(Eigen::MatrixXd &coord, const std::string& folder, const std::string& filename) {

        // set file name
        std::string path = folder + filename + ".txt";
        std::ofstream os;

        os.open(path);
        if (!os.is_open()) {
            std::cout << "Outputstream not open." << std::endl;
        }

        std::cout << "Path: " << path << std::endl;

        // write file
        for (int j=0; j<coord.cols(); ++j) {
            for (int i=0; i<coord.rows(); ++i) {
                if (i!=coord.rows()-1) {
                    os << coord(i,j) << " ";
                }
                else {
                    if (j!=coord.cols()-1)
                        os << coord(i,j) << std::endl;
                    else
                        os << coord(i,j);
                }
            }
        }
        os.close();
    }

    void save_ray_traversals_(std::vector<std::vector<int> > &ray_traversals_, const std::string& folder, const std::string& filename) {

        // set file name
        std::string path = folder + filename + ".txt";
        std::ofstream os;

        os.open(path);
        if (!os.is_open()) {
            std::cout << "Outputstream not open." << std::endl;
        }

        std::cout << "Path" << path << std::endl;

        // write file
        std::cout << ray_traversals_.size() << std::endl;
        for (int i=0; i<ray_traversals_.size(); ++i) {
            if (ray_traversals_[i].size()==0)
                os << "-1" << std::endl;
            for (int j=0; j<ray_traversals_[i].size(); ++j) {
                if (j!=ray_traversals_[i].size()-1) {
                    os << ray_traversals_[i][j] << " ";
                }
                else {
                    if (i!=ray_traversals_.size()-1)
                        os << ray_traversals_[i][j] << " -1" << std::endl;
                    else
                        os << ray_traversals_[i][j] << " -1";
                }
            }
        }
        os.close();
    }

    void save_ray_depths_(std::vector<int> &ray_depths_, const std::string& folder, const std::string& filename) {

        // set file name
        std::string path = folder + filename + ".txt";
        std::ofstream os;

        os.open(path);
        if (!os.is_open()) {
            std::cout << "Outputstream not open." << std::endl;
        }

        // write file
        for (int i=0; i<ray_depths_.size(); ++i) {
            if (i!=ray_depths_.size()-1)
                os << ray_depths_[i] << std::endl;
            else
                os << ray_depths_[i];
        }
        os.close();
    }

    void save_neighbors_(std::vector<std::array<int,4>> &neighbors_, const std::string& folder, const std::string& filename) {

        // set file name
        std::string path = folder + filename + ".txt";
        std::ofstream os;

        os.open(path);
        if (!os.is_open()) {
            std::cout << "Outputstream not open." << std::endl;
        }

        // write file
        for (int i=0; i<neighbors_.size(); ++i) {
            for (int j=0; j<4; ++j) {
                if (j!=3) {
                    os << neighbors_[i][j] << " ";
                }
                else {
                    if (i!=neighbors_.size()-1)
                        os << neighbors_[i][j] << std::endl;
                    else
                        os << neighbors_[i][j];
                }

            }
        }
        os.close();
    }
    void save_neighbors_surface_(std::vector<std::array<double,4>> &neighbors_surface_,
                                 const std::string& folder, const std::string& filename){
        // set file name
        std::string path = folder + filename + ".txt";
        std::ofstream os;

        os.open(path);
        if (!os.is_open()) {
            std::cout << "Outputstream not open." << std::endl;
        }

        // write file
        for (int i=0; i<neighbors_surface_.size(); ++i) {
            for (int j=0; j<4; ++j) {
                if (j!=3) {
                    os << neighbors_surface_[i][j] << " ";
                }
                else {
                    if (i!=neighbors_surface_.size()-1)
                        os << neighbors_surface_[i][j] << std::endl;
                    else
                        os << neighbors_surface_[i][j];
                }

            }
        }
        os.close();
    }


    void write_readme(const int NUM_FINITE_CELLS, const int NUM_RAYS, const int NUM_VERTICES, const std::string& folder) {

        // set file name
        std::string path = folder + "README.txt";
        std::ofstream os;

        os.open(path);
        if (!os.is_open()) {
            std::cout << "Outputstream not open." << std::endl;
        }

        // write file
        os << "Number of cells    : " << NUM_FINITE_CELLS << std::endl;
        os << "Number of rays     : " << NUM_RAYS << std::endl;
        os << "Number of vertices : " << NUM_VERTICES << std::endl << std::endl;

        os << "ray_traversals_      : The indices of cells along each ray." << std::endl;
        os << "                      (Num rays) x (Num traverses) (Note: Each ray has different Number of travere indices)" << std::endl << std::endl;

        os << "ray_depths_          : The indices of cells where transition happens" << std::endl;
        os << "                      (Num rays) x (1)" << std::endl << std::endl;

        os << "neighbors            : The indices of neighbor cells for every cell" << std::endl;
        os << "                      (Num cells) x (4) (Note: All infinite cells have the index -1)" << std::endl << std::endl;

        os << "index_vertex_per_cell: The indices of the vertices that form cells" << std::endl;
        os << "                      (Num cells) x (4)" << std::endl << std::endl;

        os << "coord_vetex          : The 3D coordinates of all the vertices" << std::endl;
        os << "                      (Num Vertexes) x (3)";

    }

    const std::vector<Vertex> sample_pointcloud(std::vector<Vertex> pcd_original, int STEP_SAMPLING){

        std::vector<Vertex> pcd_sampled;

        for (int i=0; i<pcd_original.size(); i+=STEP_SAMPLING)
            pcd_sampled.push_back(pcd_original[i]);

        return pcd_sampled;
    }

    void assign_vertex_index(std::vector<Vertex> &pcd) {

        for (int i=0; i<pcd.size(); ++i) {
            pcd[i].set_index_vertex(i);
        }
    }

    const int assign_finite_cell_index(Delaunay &T) {

        Cells_iterator cit;
        int index_cell = 0;

        // iterate every finite cell
        for (cit = T.finite_cells_begin(); cit != T.finite_cells_end(); ++cit) {
            // assign an index for every finite cell
            cit->info() = index_cell;
            ++index_cell;
        }
        return index_cell;
    }

    const int assign_all_cell_index(Delaunay &T) {
        All_cells_iterator cit;
        int index_cell = -1;
        int count = 0;

        // iterate all the cells
        for (cit=T.all_cells_begin(); cit!= T.all_cells_end(); ++cit) {
            // assign an index -1 for every cell
            cit->info() = index_cell;
            ++count;
        }
        return count;
    }

    const std::vector<int> get_index_on_bbox (Eigen::MatrixXd &coord_vertex, Eigen::Matrix<double,2,3> &bbox, Eigen::MatrixXi &index_vertex_per_cell) {

        std::vector<int> index_vertex_on_bbox;
        std::vector<int> index_cell_on_bbox;


        for (int i=0; i<coord_vertex.cols(); ++i) {
            if ((int)coord_vertex(2,i)>=(int)bbox(1,2)-23) {
                index_vertex_on_bbox.push_back(i);// z=90
            }
            if ((int)coord_vertex(0,i)<=(int)bbox(0,0)+10) {
                index_vertex_on_bbox.push_back(i);// x=-116
            }
            if ((int)coord_vertex(0,i)>=(int)bbox(1,0)-10) {
                index_vertex_on_bbox.push_back(i);// x=140
            }
            if ((int)coord_vertex(1,i)<=(int)bbox(0,1)+8  ) {
                index_vertex_on_bbox.push_back(i);//y -65
            }
            if ((int)coord_vertex(1,i)>=(int)bbox(1,1)-18) {
                index_vertex_on_bbox.push_back(i);//y=90
            }
            if ((int)coord_vertex(0,i)>=(int)bbox(1,0)-38 && (int)coord_vertex(2,i)>=(int)bbox(1,2)-58 ) {
                index_vertex_on_bbox.push_back(i);// upper right block
            }
        }

        sort( index_vertex_on_bbox.begin(), index_vertex_on_bbox.end() );
        index_vertex_on_bbox.erase( unique( index_vertex_on_bbox.begin(), index_vertex_on_bbox.end() ), index_vertex_on_bbox.end() );

        for (int j=0; j<index_vertex_per_cell.cols(); ++j) {
                    int count = 0;
                    for (int i=0; i<4; ++i) {
                        int index_vertex_on_cell = index_vertex_per_cell(i,j);

                        for (int k=0; k<index_vertex_on_bbox.size(); ++k) {
                            if (index_vertex_on_cell == index_vertex_on_bbox[k]) {
        //                        index_cell_on_bbox.push_back(j);
        //                        break;
                                ++count;
                                break;
                            }
                        }
                    }

                    if (count >= 1 ) {
//                        inside_and_onbbox = true;
//                        for (int i=0; i<4; ++i) {
//                            int index_vertex_on_cell = index_vertex_per_cell(i,j);
//                            if ((int)coord_vertex(index_vertex_on_cell,i)>(int)bbox(1,2)) {
//                                inside_and_onbbox = false;
//                            }
//                        }
//                        if (inside_and_onbbox) {
                            index_cell_on_bbox.push_back(j);

//                        }
                    }




                }


        return index_cell_on_bbox;

    }


    Eigen::MatrixXi get_vertex_index_per_cell(const Delaunay &T) {

        // check the number of finite cells
        int NUM_FINITE_CELLS = std::distance(T.finite_cells_begin(),T.finite_cells_end());

        Eigen::MatrixXi index_vertex_per_cell = Eigen::MatrixXi::Zero(4,NUM_FINITE_CELLS);
        Cells_iterator cit;

        // get 4 vertex indices that consist each cell
        for(cit=T.finite_cells_begin(); cit!=T.finite_cells_end(); ++cit) {
            for (int i=0; i<4; ++i){
                index_vertex_per_cell(i,cit->info()) = cit->vertex(i)->info()(1);
            }
        }
        return index_vertex_per_cell;
    }

    Eigen::MatrixXd get_vertex_coord(const Delaunay &T) {

        // check the number of finite cells
        int NUM_VERTICES = std::distance(T.finite_vertices_begin(),T.finite_vertices_end());

        Eigen::MatrixXd coord_vertex = Eigen::MatrixXd::Zero(3,NUM_VERTICES);

        Vertices_iterator vit;

        // iterate vertices and get their x,y,z values
        for (vit = T.finite_vertices_begin(); vit!=T.finite_vertices_end(); ++vit) {

            Point pt = vit->point();
            int index_pt = vit->info()(1);

            coord_vertex(0,index_pt) = pt.x();
            coord_vertex(1,index_pt) = pt.y();
            coord_vertex(2,index_pt) = pt.z();

            }
        return coord_vertex;
    }

    std::pair< std::vector<std::vector<int>>,std::vector<int> > traverse_ray(const Delaunay &T, std::vector<PSL::DepthMap<float,double> > &depthMaps) {

        // define parameters
        const int NUM_RAYS = T.number_of_vertices();
        const int NUM_FINITE_CELLS = std::distance(T.finite_cells_begin(),T.finite_cells_end());

        // initialize variables
        std::vector<std::vector<int>> ray_traversals_(NUM_RAYS);
        std::vector<int> ray_depths_;

        // get indices for every ray
        for (int idx_ray=0; idx_ray<NUM_RAYS; ++idx_ray) {

            int index_propagate;
            Cell_handle cell_propagate;
            Ray curr_ray;
            Segment curr_seg;
            int idx_vertex = idx_ray;

            Vertices_iterator vit = T.finite_vertices_begin();
            // advance vertex iterator to indicate the proper vertex
            std::advance(vit,idx_vertex);

            // get the camera center of the selected vertex
            Eigen::Vector2i info_vertex = vit->info();
            int index_cam = info_vertex(0);
            if (index_cam != -1) {
                Eigen::Matrix<double,3,1> C = (depthMaps[index_cam].getCam()).getC();
                Point coord_camera = Point(C(0,0),C(1,0),C(2,0));     // camera center

                // get the cooridnate of the selected vertex
                Point coord_vertex = vit->point();

                // construct the ray and segment from camera center to the vertex
                curr_ray = Ray(coord_camera,coord_vertex);
                curr_seg = Segment(coord_camera,coord_vertex);

                std::vector<Cell_handle> cell_incident;

                // output iterator for accessing adjacent cells
                T.finite_incident_cells(vit,std::back_inserter(cell_incident));
                std::vector<Cell_handle>::iterator it;

                // set for propagation of intersection along the ray
                bool is_ray_depths = false;
                bool is_ray_traversals = false;

                int index_secondLast;
                int index_last;

                // find intersection with the ray and the segment among adjacent cells
                for(it=cell_incident.begin(); it!=cell_incident.end(); ++it) {

                    int index_cell = (*it)->index(vit);

                    // create facets of a adjacent cell
                    Triangle curr_facet = T.triangle(*it, index_cell);

                    if (CGAL::do_intersect(curr_ray,curr_facet)) {

                        CGAL::Object result = CGAL::intersection(curr_ray,curr_facet);
                        Point point;

                        if (CGAL::assign(point,result)) {

                            // get the last index of ray_traversals_
                            if (CGAL::do_intersect(curr_seg,curr_facet)) {

                                index_secondLast = (*it)->info();

                                index_propagate = index_cell;
                                cell_propagate = *it;
                                is_ray_traversals = true;
                            }

                            // get the index of ray_depth_
                            else {

                                index_last = (*it)->info();
                                is_ray_depths = true;
                            }
                        }
                    }
                }


                if (is_ray_depths && is_ray_traversals) {
                    ray_traversals_[idx_ray].push_back(index_secondLast);
                    ray_depths_.push_back(index_last);

                    int counter = 0;

                    // further transversing the ray towards the camera
                    bool TERMINATE_TRAVERSE = false;

                    do {
                        // facet indices in the next cell
                        int index_cell = T.mirror_index(cell_propagate,index_propagate);
                        int all_indices[] = {0,1,2,3};
                        int* all_indices_start = all_indices;
                        int* all_indices_end = all_indices+sizeof(all_indices)/sizeof(int);

                        // create array of indices which correponding untested faces in new cell
                        all_indices_end = std::remove(all_indices_start,all_indices_end,index_cell);

                        ++counter;

                        // save up the index of current cell
                        ray_traversals_[idx_ray].push_back(cell_propagate->neighbor(index_propagate)->info());

                        // update cell handle
                        cell_propagate = cell_propagate->neighbor(index_propagate);

                        // setup break condition
                        TERMINATE_TRAVERSE = true;

                        // check intersecting cell
                        for (int* idx_local=all_indices_start; idx_local!=all_indices_end; ++idx_local) {

                            Triangle curr_facet =  T.triangle(cell_propagate, *idx_local);
                            CGAL::Object result = CGAL::intersection(curr_ray, curr_facet);

                            Point point;

                            if (CGAL::do_intersect(curr_ray,curr_facet) && CGAL::assign(point,result)) {

                                // Update index
                                index_propagate = *idx_local;
                                //std::cout << "Next Cell Index is " << propagate_cell->info()
                                //          << " at the face correspond to Vertex " << *local_index
                                //          << " , continue tranversing at " << TranverseCellCounter+2 << std::endl;
                                TERMINATE_TRAVERSE = false;
                                //break;
                            }

                        }

                    } while((!TERMINATE_TRAVERSE) &&
                            (cell_propagate->neighbor(index_propagate)->info() < NUM_FINITE_CELLS) &&
                            (cell_propagate->neighbor(index_propagate)->info() > 0));

                    std::reverse(ray_traversals_[idx_ray].begin(),ray_traversals_[idx_ray].end());
                }
            }
        }
        ray_traversals_.resize(ray_depths_.size());
        return std::make_pair(ray_traversals_,ray_depths_);
    }

    const std::vector< std::array<int,4> > get_neighbors_(Delaunay &T) {

        // calculate number of finite cells
        const int NUM_FINITE_CELLS = std::distance(T.finite_cells_begin(),T.finite_cells_end());

        // initialize neighbor
        std::vector< std::array<int,4> > neighbors_(NUM_FINITE_CELLS);

        // define iterator
        Cells_iterator cit;

        for (cit=T.finite_cells_begin(); cit!=T.finite_cells_end(); ++cit) {
            for (int i=0; i<4; ++i) {
                Cell_handle cell = cit->neighbor(i);
                neighbors_[cit->info()][i] = cell->info();
            }
        }
        return neighbors_;
    }

    const std::vector< std::array<double,4> > get_neighbors_surface_(Delaunay &T) {

        // calculate number of finite cells
        const int NUM_FINITE_CELLS = std::distance(T.finite_cells_begin(),T.finite_cells_end());

        // initialize neighbor
        std::vector< std::array<double,4> > neighbors_surface_(NUM_FINITE_CELLS);

        // define iterator
        Cells_iterator cit;

        // saving squared area
        for (cit=T.finite_cells_begin(); cit!=T.finite_cells_end(); ++cit) {
            for (int i=0; i<4; ++i) {
                Triangle curr_tri = T.triangle(cit, i);
                double surface_area = curr_tri.squared_area();
                neighbors_surface_[cit->info()][i] = std::min(0.0001,surface_area);// upper bound with 10e-4
            }
        }
        return neighbors_surface_;
    }


    std::vector<std::array< int,3>> get_surface(const Delaunay &T, const std::vector<int> &label) {

        std::vector<std::array<int,3>> surface ;
        Facets_iterator fit;

        // iterate all the finite facets
        for (fit=T.finite_facets_begin(); fit!=T.finite_facets_end(); ++fit){

            Facet mirrored_facet = T.mirror_facet(*fit); // Adjacent identical face
            int cell_index = ((*fit).first)->info(); // get index of corresponding cells
            int adjacent_cell_index = (mirrored_facet.first)->info(); //get index of corresponding cells

            if ((label[cell_index] + label[adjacent_cell_index] == 1) && !(T.is_infinite((*fit).first))
                    && !(T.is_infinite(mirrored_facet)) && !(T.is_infinite(mirrored_facet.first)))   { // Transition happens
                std::array <int,3> triangle_vertex_index;
                Facet handle_face;
                if (label[adjacent_cell_index] == 1) { // Triangle oriented to outside
                    handle_face = mirrored_facet;
                }
                else
                    handle_face = *fit;

                // find the index of every vertex. In correct orientation
                Cell_handle current_cell = handle_face.first;
                int face_index = handle_face.second;

                // 4 possible indices
                switch (face_index) {
                case 0:
                    triangle_vertex_index[0] = current_cell->vertex(1)->info()(1);
                    triangle_vertex_index[1] = current_cell->vertex(2)->info()(1);
                    triangle_vertex_index[2] = current_cell->vertex(3)->info()(1); break;
                case 1:
                    triangle_vertex_index[0] = current_cell->vertex(0)->info()(1);
                    triangle_vertex_index[1] = current_cell->vertex(3)->info()(1);
                    triangle_vertex_index[2] = current_cell->vertex(2)->info()(1); break;
                case 2:
                    triangle_vertex_index[0] = current_cell->vertex(0)->info()(1);
                    triangle_vertex_index[1] = current_cell->vertex(1)->info()(1);
                    triangle_vertex_index[2] = current_cell->vertex(3)->info()(1); break;
                case 3:
                    triangle_vertex_index[0] = current_cell->vertex(0)->info()(1);
                    triangle_vertex_index[1] = current_cell->vertex(2)->info()(1);
                    triangle_vertex_index[2] = current_cell->vertex(1)->info()(1); break;
                }
                surface.push_back(triangle_vertex_index);
            }
        }
        return surface;
    }

    std::vector<std::array< int,3>> get_surface_triangulation(const Delaunay &T) {

        std::vector<std::array<int,3>> surface ;
        Facets_iterator fit;   // Define finite face iterator

        for (fit=T.finite_facets_begin(); fit!=T.finite_facets_end(); ++fit){

            std::array <int,3> triangle_vertex_index;

            // find the index of every vertex in the correct orientation
            Cell_handle current_cell = (*fit).first;
            int face_index = (*fit).second;

            // 4 possible indices
            switch (face_index) {
            case 0:
                triangle_vertex_index[0] = current_cell->vertex(1)->info()(1);
                triangle_vertex_index[1] = current_cell->vertex(2)->info()(1);
                triangle_vertex_index[2] = current_cell->vertex(3)->info()(1); break;
            case 1:
                triangle_vertex_index[0] = current_cell->vertex(0)->info()(1);
                triangle_vertex_index[1] = current_cell->vertex(3)->info()(1);
                triangle_vertex_index[2] = current_cell->vertex(2)->info()(1); break;
            case 2:
                triangle_vertex_index[0] = current_cell->vertex(0)->info()(1);
                triangle_vertex_index[1] = current_cell->vertex(1)->info()(1);
                triangle_vertex_index[2] = current_cell->vertex(3)->info()(1); break;
            case 3:
                triangle_vertex_index[0] = current_cell->vertex(0)->info()(1);
                triangle_vertex_index[1] = current_cell->vertex(2)->info()(1);
                triangle_vertex_index[2] = current_cell->vertex(1)->info()(1); break;
            }
            surface.push_back(triangle_vertex_index);
        }
        return surface;
    }
}
