#ifndef VERTEX_H
#define VERTEX_H

#include <iostream>
#include <Eigen/Dense>
#include <utility>
#include "typedef.h"

namespace RS
{
    class Vertex
    {
    public:
        Vertex();
        Vertex(Point pt, int idx_cam);

        void set_point(Point pt);
        void set_index_cam(int idx);
        void set_index_vertex(int idx);

        Point get_point();
        double get_point(int i);
        Eigen::Vector2i get_index();
        int get_index_cam();
        int get_index_vertex();
        std::pair<Point, Eigen::Vector2i> get_pair();


    private:
        Point point;
        int index_cam;
        int index_vertex;
    };
}

#endif // VERTEX_H
