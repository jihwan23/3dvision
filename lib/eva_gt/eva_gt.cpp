#include "eva_gt.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

using namespace std;

namespace EV {

    void save_reconstruction_unsigned_errors(const string& ground_truth_file_name, const string& result_file_name) {

        // Define points and structure
        typedef float T;
        string accuracy_file_name = result_file_name + "_accuracy.txt";
        string completeness_file_name = result_file_name + "_completeness.txt";
        vector<vector<T> > ground_truth_points;
        vector<vector<T> > result_points;
        vector<vector<T> > all_result_points;
        string line;
        ifstream file;

        // read ground truth file in PLY
        file.open(ground_truth_file_name);
        bool is_reading_points = false;
        while (getline(file, line)) {
            stringstream chunks(line);
            if (!is_reading_points) {
                string first_chunk;
                chunks >> first_chunk;
                if (first_chunk == "end_header") {
                    is_reading_points = true;
                }
            }
            else {
                T x, y, z;
                chunks >> x >> y >> z;
                ground_truth_points.push_back(vector<T>{x, y, z});
                if (x==3) break;
            }
        }
        file.close();

        // read result file result file is in obj
        file.open(result_file_name);
        std::vector<int> vertex_used;
        while (getline(file, line)) {
            stringstream chunks(line);
            string prefix;
            chunks >> prefix;
            if (prefix == "v") {
                T x, y, z;
                chunks >> x >> y >> z;
                all_result_points.push_back(vector<T>{x, y, z});
            }
            else if (prefix == "f") {
                T x,y,z;
                chunks >> x >> y >> z;
                vertex_used.push_back(x);
                vertex_used.push_back(y);
                vertex_used.push_back(z);
            }
        }

        std::cout << "Finish reading, now find vertices used" << std::endl;

        set<int> s(vertex_used.begin(),vertex_used.end());
        vertex_used.assign(s.begin(),s.end());
        std::cout << vertex_used.size() << std::endl;

        for(int i = 0; i< vertex_used.size() ; i++) {
            int vertex_index = vertex_used.at(i);
            result_points.push_back(all_result_points.at(vertex_index-1));
        }
        file.close();


        // reorder for flann
        flann::Matrix<T> result_matrix(new T[result_points.size() * 3], result_points.size(), 3);
        flann::Matrix<T> ground_truth_matrix(new T[ground_truth_points.size() * 3], ground_truth_points.size(), 3);
        for (int result_index = 0; result_index < result_points.size(); ++result_index) {
            result_matrix[result_index][0] = result_points[result_index][0];
            result_matrix[result_index][1] = result_points[result_index][1];
            result_matrix[result_index][2] = result_points[result_index][2];
        }
        for (int ground_truth_index = 0;
             ground_truth_index < ground_truth_points.size();
             ++ground_truth_index) {
            ground_truth_matrix[ground_truth_index][0] = ground_truth_points[ground_truth_index][0];
            ground_truth_matrix[ground_truth_index][1] = ground_truth_points[ground_truth_index][1];
            ground_truth_matrix[ground_truth_index][2] = ground_truth_points[ground_truth_index][2];
        }


        // accuracy
        {
            vector<float> accuracy_score ;
            flann::Matrix<int> indices(new int[result_matrix.rows], result_matrix.rows, 1);
            flann::Matrix<T> dists(new T[result_matrix.rows], result_matrix.rows, 1);
            flann::Index<flann::L2<T> > index(ground_truth_matrix, flann::KDTreeSingleIndexParams());
            index.buildIndex();
            index.knnSearch(result_matrix, indices, dists, 1, flann::SearchParams());
            ofstream output;
            output.open(accuracy_file_name);
            output.precision(std::numeric_limits<float>::digits10 + 1);

            for (int result_index = 0; result_index < result_points.size(); ++result_index) {
                output << sqrt(dists[result_index][0]) << endl;
                accuracy_score.push_back(sqrt(dists[result_index][0]));
            }
            sort(accuracy_score.begin(),accuracy_score.end());
            int ninety_percent = round(0.9*result_points.size());
            float ninety_percent_error = accuracy_score.at(ninety_percent);
            cout << "The accuracy score at 90% is " << ninety_percent_error << endl;

            output << ninety_percent_error << endl;
            output.close();
            delete[] indices.ptr();
            delete[] dists.ptr();
        }


        // completeness
        {
            flann::Matrix<int> indices(new int[ground_truth_matrix.rows], ground_truth_matrix.rows, 1);
            flann::Matrix<T> dists(new T[ground_truth_matrix.rows], ground_truth_matrix.rows, 1);
            flann::Index<flann::L2<T> > index(result_matrix, flann::KDTreeSingleIndexParams());
            index.buildIndex();
            index.knnSearch(ground_truth_matrix, indices, dists, 1, flann::SearchParams());
            ofstream output;
            output.open(completeness_file_name);
            output.precision(std::numeric_limits<float>::digits10 + 1);
            int count_complete = 0;
            for (int ground_truth_index = 0; ground_truth_index < ground_truth_points.size(); ++ground_truth_index) {
                // output << sqrt(dists[ground_truth_index][0]) << endl; % too big output file
                if (sqrt(dists[ground_truth_index][0]) < 0.1) {
                    count_complete+=1;
                }
            }
            double com_score= count_complete/(double)ground_truth_points.size()*100;
            cout << "The completeness under 0.1 mm is " <<  com_score << "%" << endl;
            output << com_score << endl;
            output.close();
            delete[] indices.ptr();
            delete[] dists.ptr();
        }
        delete[] result_matrix.ptr();
        delete[] ground_truth_matrix.ptr();
    }

}
